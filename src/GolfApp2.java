//Created by Dmitry Vereykin

public class GolfApp2 {
    public static void main(String[] args) {
      
        BSTInterface<Golfer> golfers = new BinarySearchTree<Golfer>();
        int numGolfers;

        golfers.add(new Golfer("Danny", 123));
        golfers.add(new Golfer("John", 432));
        golfers.add(new Golfer("Alice", 222));
        golfers.add(new Golfer("Robert", 96));
        golfers.add(new Golfer("Randy", 176));

        System.out.println();
        System.out.println("The final results are: ");

        numGolfers = golfers.reset(BinarySearchTree.INORDER);

        for (int count = 1; count <= numGolfers; count++) {
         System.out.println(golfers.getNext(BinarySearchTree.INORDER));
        }

        System.out.println("\nNumber of nodes less than 200 --> " + countLess((BinarySearchTree) golfers, new Golfer("Jimmy", 200)));
        System.out.println("Minimum --> " + min((BinarySearchTree) golfers));

    }

    public static int countLess(BinarySearchTree<Golfer> tree , Golfer maxValue) {
        int numGolfers = tree.reset(BinarySearchTree.INORDER);
        int value = maxValue.getScore();
        int counter = 0;

        for (int count = 1; count <= numGolfers; count++) {
            if (tree.getNext(BinarySearchTree.INORDER).getScore() <= value) {
                counter++;
            }
        }

        return counter;
    }

    public static Golfer min(BinarySearchTree<Golfer> tree) {
        int numGolfers = tree.reset(BinarySearchTree.INORDER);
        Golfer tempGolfer;
        Golfer minGolfer = tree.getNext(BinarySearchTree.INORDER);

        for (int count = 2; count <= numGolfers; count++) {
            tempGolfer = tree.getNext(BinarySearchTree.INORDER);

            if (tempGolfer.getScore() < minGolfer.getScore()) {
                minGolfer = tempGolfer;
            }
        }

        return minGolfer;
    }
}